import { Component,signal } from '@angular/core';
import{CommonModule} from '@angular/common';// Replace 'some-library' with the actual library or module name.
import{FormControl, ReactiveFormsModule, Validators} from '@angular/forms';

import{Task} from './../../models/task.model'
import { Title } from '@angular/platform-browser';
import { single } from 'rxjs';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  tasks = signal<Task[]>([
    {
      id: Date.now(),
      title: "crear proyecto",
      completed: false

    },
    {
      id: Date.now(),
      title: "crear componentes",
      completed: false

    }

  ]);

  newtaskControl = new FormControl('',{
    nonNullable:true,
    validators:[
      Validators.required,
      Validators.pattern('^\\S.*$'),

    ]

  }

  )

  changeHandle(){
    if(this.newtaskControl.valid){
      const value=this.newtaskControl.value.trim();
      this.addtask(value);
      this.newtaskControl.setValue('');
    }


  }

  addtask(title: string){
    const newTask={
      id:Date.now(),
      title,
      completed: false
    };
    this.tasks.update((tasks) =>[...tasks,newTask]);

  }



  keyDouwnHundle(event:KeyboardEvent){
    const input=event.target as HTMLInputElement;
    console.log(input.value);

  }

  deleteTask(index:number){
    this.tasks.update((tasks) => tasks.filter((task, position) => position!==index));
  }
/*
  updateTask(index: number){
    this.tasks.update((tasks) => {
      return tasks.map((task,position) => {
        if(position === index){
          return{
            ...task,
            completed: !task.completed
          }
        }
        return task;
      })
    })
  }*/
updateTask(index:number){
  this.tasks.update((tasks) => {
    return tasks.map((task,position) =>{
      if(position === index){
        return{
        ...task,
        completed: !task.completed
      }}
      return task;
    })
  })

}



}
