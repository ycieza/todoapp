import { Component,signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import{Task} from './../../models/task.model'
import { __values } from 'tslib';

@Component({
  selector: 'app-labs',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule],
  templateUrl: './labs.component.html',
  styleUrl: './labs.component.css'
})
export class LabsComponent {
  welcome = 'Hola!';

  tasks = signal ([
    'Instalar Cli',
    'Instalar comandos',
    'Crear proyectos'
  ]);
  name = signal('Yober');
 age = 17;

 disabled=true;

 img='https://w3schools.com/howto/img_avatar.png';

 person=signal({
  name:'Yober',
  age:17,
  avatar:'https://w3schools.com/howto/img_avatar.png'
  })

  perro ={
    name:'Boby',
    age:5,
    raza:'Snauser',
    img:'https://www.purina.es/sites/default/files/styles/ttt_image_original/public/2021-02/BREED%20Hero%20Desktop_0033_schnauzer_standard.webp?itok=3oe6Y5QZ'

  }
colorControl =new FormControl();
constructor(){
  this.colorControl.valueChanges.subscribe(value =>{
    console.log(value)

  })
}
  disabledPerro=false;

  clickHandler(){
    alert('Hola');
  }
  changeHandle(event:Event){
    const input=event.target as HTMLInputElement;
    const newValue=input.value;
    this.name.set(newValue)
    console.log(newValue);


  }
  keyDouwnHundle(event:KeyboardEvent){
    const input=event.target as HTMLInputElement;
    console.log(input.value);

  }

  changeAge(event:Event){
    const input=event.target as HTMLInputElement
    const newValue=input.value;
    this.person.update(prevState =>{
      return{
        ...prevState,
        age:parseInt(newValue,10)
      }
    })


  }

  changeName(event:Event){
    const input=event.target as HTMLInputElement
    const newValue=input.value;
    this.person.update(prevState =>{
      return{
        ...prevState,
        name: newValue
      }
    })


  }

}
